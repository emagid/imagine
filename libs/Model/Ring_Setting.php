<?php
/**
 * Created by PhpStorm.
 * User: Simon
 * Date: 11/23/2015
 * Time: 4:33 PM
 */

namespace Model;

class Ring_Setting extends \Emagid\Core\Model {

//    static $tablename = 'ring_setting';

    public static $shape = [
        '4 Prong' => 1,
        '6 Prong' => 2,
//        'Antique' => 3
    ];

    public static function featureImage($id)
    {
        switch($id){
            case 1: return FRONT_ASSETS.'img/ringfourprong_720.jpg';
            case 2: return FRONT_ASSETS.'img/ringsixprong_720.jpg';
//            case 3: return FRONT_ASSETS.'img/ring6.png';
            default: return FRONT_ASSETS.'img/ring10.png';
        }
    }

    public static function getName($id)
    {
        return array_search($id, self::$shape);
    }
}