<?php

namespace Model;

class Session extends \Emagid\Core\Model {
    static $tablename = "public.session";

    public static $fields  =  [
        'name',
        'position',
        'title',
        'subtitle',
        'date_time',
        'text',
        'headshot',
        'logo'
    ];

}