<?php

class sessionsController extends adminController {
	
	function __construct(){
		parent::__construct("Session", "sessions");
	}

	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		
		$this->_viewData->hasLoadBtn = true;
        $params['queryOptions']['orderBy'] = 'date_time ASC';

		parent::index($params);
	}

	function update(Array $arr = []){

		parent::update($arr);
	}

	function update_post(Array $arr = []){
		parent::update_post($arr);
	}

	function import_post(Array $arr = []) {
		if($_FILES['file']['error'] > 0){
            $n = new \Notification\ErrorHandler('File is invalid');
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL."sessions/");
        } else {
        	$mFile = fopen($_FILES["file"]["tmp_name"],'r');
            $header    = fgetcsv($mFile);

            while($row = fgetcsv(mFile)) {

            }
        }
		// redirect(ADMIN_URL.'/sessions');
	}

    function readCSV_post(){
        parent::readCSV_post();

        $imported = 0;
        $updated = 0;
        if (! function_exists('array_column')) {
            function array_column(array $input, $columnKey, $indexKey = null) {
                $array = array();
                foreach ($input as $value) {
                    if ( !array_key_exists($columnKey, $value)) {
                        trigger_error("Key \"$columnKey\" does not exist in array");
                        return false;
                    }
                    if (is_null($indexKey)) {
                        $array[] = $value[$columnKey];
                    }
                    else {
                        if ( !array_key_exists($indexKey, $value)) {
                            trigger_error("Key \"$indexKey\" does not exist in array");
                            return false;
                        }
                        if ( ! is_scalar($value[$indexKey])) {
                            trigger_error("Key \"$indexKey\" does not contain scalar value");
                            return false;
                        }
                        $array[$value[$indexKey]] = $value[$columnKey];
                    }
                }
                return $array;
            }
        }
        $mFile = fopen($_FILES["sessions"]["tmp_name"],'r');
        $header = fgetcsv($mFile);
        while($row = fgetcsv($mFile)){
            $import = false;
            $_session = array_combine($header, $row);
            $dateString = $_session['Date'].' 2018 '.$_session['Time'].' EST';
            $_session['date_time'] = strtotime($dateString);

            $session = \Model\Session::getItem(null,['where'=>"name = '{$_session['Name']}' AND date_time = {$_session['date_time']}"]);
            if(!$session) {
                $import = true;
                $session = new \Model\Session();
            }
            foreach ($_session as $key => $value){
                $_key = strtolower($key);
                $session->$_key = $value;
            }
            if($session->save()){
                if($import){
                    $imported++;
                } else {
                    $updated++;
                }
            }
        }
        fclose($mFile);
        $n = new \Notification\MessageHandler("$imported $this->_content imported <br />$updated $this->_content updated ");
        $_SESSION["notification"] = serialize($n);
        redirect(ADMIN_URL . $this->_content);

    }

}