<?php
 
class BannersController extends adminController {
	
	function __construct(){
		parent::__construct("Banner", "banners");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		


		parent::index($params);
	}

	function update(Array $arr = []){
		 
      
		parent::update($arr);
	}
  
}