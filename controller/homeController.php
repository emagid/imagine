<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    function photobooth1(Array $params = []){
        $this->loadView($this->viewData);
    }

    function photobooth2(Array $params = []){
        $this->loadView($this->viewData);
    }

    function about(Array $params = []){
        $this->viewData->aboutus = \Model\About_Us::getList(['where'=>"active = 1"]);
        $this->loadView($this->viewData);
    }

    function map(Array $params = []){
        $this->viewData->map1 = \Model\Map::getItem(null,['where'=>"display_order = '1'"]);
        $this->viewData->map2 = \Model\Map::getItem(null,['where'=>"display_order = '2'"]);
        $this->viewData->map3 = \Model\Map::getItem(null,['where'=>"display_order = '3'"]);
        $this->viewData->map4 = \Model\Map::getItem(null,['where'=>"display_order = '4'"]);
    
        $this->loadView($this->viewData);
    }

    function highlights(Array $params = []){
        $this->viewData->highlights = \Model\Highlight::getList(['where'=>"active = 1"]);
        $this->loadView($this->viewData);
    }

    function support(Array $params = []){
        $this->viewData->supports = \Model\Support::getList(['where'=>"active = 1", 'orderBy'=>"id asc"]);
        $this->loadView($this->viewData);
    }

    function support_post(){

        $sid = TWILIO_SID;
        $token = TWILIO_TOKEN;
        $client = new Client($sid, $token);
        $from = TWILIO_NUMBER;
        
        if(isset($_POST['fname']) && isset($_POST['supportList']) && $_POST['fname'] != "" && $_POST['supportList'] != ""){
            
            $support_list =  $_POST['supportList'];
            $support = \Model\Support::getItem($support_list);
            
            //Number to which the message should be sent.
            $toSupport = $support->phone;
            $user_contact = $_POST['fname'];

            $user = new \Model\Support_User;
            $user->user_phone = $user_contact;
            $user->support_list_id = $support_list;
            
            try{
                $phone_number = $client->lookups->v1->phoneNumbers($user_contact)
                    ->fetch(array(
                            'addOns' => "whitepages_pro_caller_id"
                        )
                    );

                if($phone_number->addOns['status'] == 'successful'){

                    if($user->save()){
                        //Sending text to Support
                        $client->messages
                        ->create(
                            $toSupport,
                            array(
                                "from" => $from,
                                "body" => "Cognizant HealthCare Conference - The following user wants to contact support team regarding category '" . $support->support_list . "' . User's contact number:". $user_contact,
                            )
                        );

                        //Sending text to user
                        $client->messages
                        ->create(
                            $user_contact,
                            array(
                                "from" => $from,
                                "body" => "Cognizant HealthCare Conference - We received your request. We'll get back to you shortly. #CognizantHCC"
                            )
                        );
                    }
                    else{
                        $n = new \Notification\ErrorHandler($user->errors);
                        $_SESSION['notification'] = serialize($n);
                    }
                }else{
                    $n = new \Notification\ErrorHandler($user->errors);
                        $_SESSION['notification'] = serialize($n);
                }

            }catch(Exception $e){
                if($e->getStatusCode() == 404) {
                    $n = new \Notification\ErrorHandler("<p class='error'>Please Enter Valid Phone Number</p>");
                    $_SESSION['notification'] = serialize($n);
                    redirect(SITE_URL.'home/support');
                }
                else{
                    $n = new \Notification\ErrorHandler("<p class='error'>Something went wrong...Please try agin with valid phone number!!</p>");
                    $_SESSION['notification'] = serialize($n);
                    redirect(SITE_URL.'home/support');
                }
            }
            
        }

        redirect('/');

    }

    function schedule(){
            $this->sessions();
    }

    function sessions(){
        $sessionSql = "SELECT to_char(to_timestamp(date_time)::timestamp without time zone, 'DAY') AS day,
                        id,
                        date_part('epoch',date_trunc('day',to_timestamp(date_time)::timestamp without time zone)) AS date
                        FROM session WHERE active = 1 ORDER BY date_time";
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sessionSql);
        $sessions = [];
        foreach($results as $result){
            if(!isset($sessions[$result['date']])){
                $sessions[$result['date']] = [
                    'day' => $result['day'],
                    'sessions' => []
                ];
            }
            $session = \Model\Session::getItem($result['id']);
            $sessions[$result['date']]['sessions'][] = $session;
        }

        $this->viewData->sessions = $sessions;
        $this->loadView($this->viewData);
    }

    function initialize(Array $params = []){
            $kiosk = $params['kiosk_id'];
            ?>
        <script type="text/javascript">
            <? if($kiosk){?>
            localStorage.kiosk = '<?=$kiosk?>';
            <? } ?>
            window.location.href = '<?=SITE_URL?>';
        </script>
        <?
    }
}