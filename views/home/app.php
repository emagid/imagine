<main>
	<section class="home" style='overflow: auto;'>

        <header class="inner_page">
<!--            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo.jpg"></a>-->
           <!--  <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><p style="color:#343142;">Go Home</p><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background background_maps'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content app'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <p class='large_intro float_left' style="color:#000;">DOWNLOAD THE APP</p>
                    </div>
                    <div class="schedule_buttons">
                        <div class="day active" id="day_one_trig">
                            <p>Hotel</p>
                        </div>
                        <div class="day " id="day_two_trig">
                            <p>Solution Center</p>
                        </div>
                        <div class="day" id="day_three_trig">
                            <p>Lower Level</p>
                        </div>
                        <div class="day" id="day_four_trig">
                            <p>Lobby Level</p>
                        </div>

                    </div>
                
                <div class="schedule_sessions" id="day_one">
                    <img width='800' src="<?=FRONT_ASSETS?>img/app.png">
                </div>
                
            </section>

<script>
$('.day').click(function() {
    $('.day').removeClass("active");
    $(this).addClass("active");
    
});
$('.day#day_one_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_one').show(); 
});
$('.day#day_two_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_two').show(); 

    $('.home').animate({
        scrollTop: $("#day_two").offset().top
    }, 1000);
});
$('.day#day_three_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_three').show(); 
});
$('.day#day_four_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_four').show(); 
});
</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                                <a href="/home/scan">
                    <img src="<?=FRONT_ASSETS?>img/qr-code.png">
                    <p>RESOURCES</p>
                </a>
                <a href="/home/about">
                    <img src="<?=FRONT_ASSETS?>img/trivia.png">
                    <p>ABOUT US</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>



