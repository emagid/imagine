<main class='home_page'>
	<section class="home">

<!--
        <div class='screensaver'>
            <div class='click_here'>
                <p class='no_ads'>CLICK BELOW TO START</p>
                <div class='start_page'></div>
                <img class='circle circle1' src="<?= FRONT_ASSETS ?>img/power.png">
                <div class='circle circle2'></div>
                <div class='circle circle3'></div>
            </div>
        </div>
-->

        <header>
<!--            <a href='/'><img src="<//?=FRONT_ASSETS?>img/logo.jpg"></a>-->
        </header> 
        <img id='logo' src="<?=FRONT_ASSETS?>img/imagine_logo.png">
        <div id='background' class='background background_home'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content choice_content_home'>
                <div id='home_screen'>
                    <img id='home_img' src="<?=FRONT_ASSETS?>img/screensaver.png">
                </div>

                <div class='choices'>
                    <a href="/home/sessions">Agenda</a>
                    <a href="/home/map">Maps</a>
                    <a href="/home/app">Download The App</a>
                </div>
                
                <div class="hashtag">
                    <h3>#ImagineNewYork18</h3>
                </div>
                
                    <div class="sponsor_contain">
                            <div class='sponsor_logos' style="display:none;">
<!--                                PREMIER-->
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/change.png">
                                </div>
                                <div>
                                    <img style='height: 110px; max-width: 100% !important; max-height: 100%;' src="<?=FRONT_ASSETS?>img/sponsors/microsoft.png">
                                </div>
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/zelis.png">
                                </div>
<!--                                PLATINUM-->
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/ibm.png">
                                </div>
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/redcard.png">
                                </div>
<!--                                GOLD ELITE-->
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/cisco.png">
                                </div>
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/informatica.png">
                                </div>     
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/onbase.png">
                                </div>    
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/optum.png">
                                </div> 
<!--                                GOLD-->
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/flextech.png">
                                </div> 
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/microdyn.png">
                                </div>
                                    <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/wex.png">
                                </div> 
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/pure-storage.png">
                                </div>
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/venture.png">
                                </div>         
                                
<!--                                SILVER-->
                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/3m.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/aci.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/adobe.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/dell.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/vmware.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/delphix.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/evolvespm.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/healthwise.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/mcg.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/phia.png">
                                </div>
                                
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/splunk.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/tibco.png">
                                </div>
                                                                <div>
                                    <img src="<?=FRONT_ASSETS?>img/sponsors/validic.png">
                                </div>

                            </div> 
                    </div>
            </section>

<script>
$('.sponsor_logos').slick({
    speed: 1500,
    autoplay: true,
    autoplaySpeed: 0,
    centerMode: false,
    cssEase: 'linear',
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: true,
    initialSlide: 1,
    arrows: false,
    buttons: false,
      responsive: [
              {
      breakpoint: 1100,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
   });

    $('.start_page').click(function(){
        $('.circle1').css('transform', 'scale(1.1)');
        setTimeout(function(){
            $('.circle1').css('transform', 'scale(1)');
        }, 300);
        setTimeout(function(){
            $('.screensaver').fadeOut();
        }, 600);
    });

    
    

</script>
           

            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/camera_w.png"> 
                    <p>PHOTOBOOTH</p>
                </a>
                <a href="/home/scan">
                    <img src="<?=FRONT_ASSETS?>img/qr-code.png">
                    <p>RESOURCES</p>
                </a>
                <a href="/home/about">
                    <img src="<?=FRONT_ASSETS?>img/trivia.png">
                    <p>ABOUT US</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
<script type="text/javascript">
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('#video').removeClass('small_img');
            $('#share_alert').removeClass('small_check');
            $('#share_alert img').removeClass('small_check_img');
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);

            setTimeout(function(){
                $('#video').addClass('small_img');
                $('#share_alert').addClass('small_check');
                $('#share_alert img').addClass('small_check_img');
            }, 3500);

            
        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });



</script>


