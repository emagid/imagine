<main>
	<section class="home speakers_page">

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo.jpg"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <p class='large_intro float_left'>SPEAKERS</p>
                        <a href="/" class="home_btn"><p>Back</p></a>
                    </div>
                </div>

                <div class='choices speakers'>
                    <a href=""><img src="<?=FRONT_ASSETS?>img/vonn.png">Lindsey Vonn
                    <span class="far_right plus">+</span>
                     <span class="far_right minus">-</span>
                    <p class="normal bio">Lindsey Vonn is the most successful female alpine skier in the history of the sport and is on her way to becoming the most successful ski racer ever.
                    <br><br>
                    Vonn produced her first World Cup victory in Lake Louise, Alberta. In 2010, Vonn became the only American woman to have captured Downhill gold at the Olympics and added a bronze in Super G. After rehabilitating two back-to-back knee injuries, Vonn returned to competition December, 2014. On January 19th, 2015 she broke Annemarie Moser-Proell’s 35-year-old record of 62 World Cup victories. In the 2018 Winter Olympics, Vonn took home a bronze medal in the Downhill competition.
                    <br><br>
                    Over the course of her career, she has captured a world-record 81 World Cup wins en route to 20 FIS Alpine World Cup titles (surpassing Sweden’s Ingemar Stenmark), including four overall crystal globes and two World Championship titles. In 2016, Vonn broke the all-time Downhill record (42 wins); claiming the season’s Downhill title outright, on top of achieving the sport’s Super-G record (28 wins).
                    <br><br>
                    Vonn launched the Lindsey Vonn Foundation in 2015. Staying true to Lindsey’s values, LVF is a non-profit with the goal of empowering young girls to grow as athletes and individuals through opportunities and experiences that build self-esteem and create positive self-image. In November 2016, People magazine named Lindsey and LVF to its “25 Women Changing the World” list. Recently added to The New York Times’ Best Sellers List, Lindsey’s book, Strong is the New Beautiful, debuted in October 2016.</p>
                    </a>
                    <a href="/home/about"><img src="<?=FRONT_ASSETS?>img/spitz.png">Judy Spitz
                    <span class="far_right plus">+</span>
                        <span class="far_right minus">-</span>
                        <p class="normal bio">Judith Spitz is a recognized leader in the communications industry, and former CIO for Verizon Enterprise Services. She’s focused on challenging organizations to develop a culture of inclusive leadership.
                        <br><br>
                        Over a 30 year career, Dr. Judith Spitz navigated the waters of a tech career starting as a member of technical staff in an Artificial Intelligence R&D Lab to a 10 year stint as Chief Information Officer at a Fortune 10 company. She is a recognized leader in the communications industry and a proven senior executive with a track record of leading complex transformational programs as part of some of the largest corporate mergers and technology deployments in U.S. history. Dr. Spitz has deployed one of the country’s first enterprise-scale call center automation systems, led the delivery of the systems infrastructure to support the nation’s first residential all fiber optic network, and set the strategy agenda for a global IT organization including the evolution from a traditional software development shop to an agile, devops-driven innovation engine focused on positioning IT as a competitive advantage for the firm.
                        <br><br>
                        She founded WiTNY (Women in Technology and Entrepreneurship in New York), a unique public/private partnership that has brought together dozens of corporate partners with two flagship academic institutions with the goal of increasing the number of undergraduate women pursuing education and careers in technology.
                        <br><br>
                        Dr. Spitz has been named to InfoWorld’s CTO 25, Computerworld’s CIO 100, CIO Magazine’s Ones to Watch list and is recipient of several leadership and entrepreneurship awards.</p>
                    </a>
                    <a href="/home/schedule"><img src="<?=FRONT_ASSETS?>img/topol.png">Dr. Eric Topol
                    <span class="far_right plus">+</span>
                        <span class="far_right minus">-</span>
                    <p class="normal bio">Eric is an author, Founder and Director of Scripps Translational Science Institute (STSI) and the Dean of Digital Medicine and preeminent speaker on the future of healthcare.
                    <br><br>
                    Dr. Eric Topol is one of the leading innovators in medicine today, specializing in the use of artificial intelligence, “deep” data, and smart technology in the practice of individualized medicine. Also called precision medicine, individualized medicine tailors diagnosis, prevention, and treatment to the full biological and social profile of the individual, rather than the “average” human of traditional medical science.
                    <br><br>
                    Dr. Eric Topol is the Founder and Director of The Scripps Translational Science Institute. He also is Executive Vice President of The Scripps Research Institute and Professor of Molecular Medicine. Before coming to Scripps to create its individualized medicine initiatives, Dr. Topol led the Cleveland Clinic to become the #1 center for heart care in the country and founded its medical school. He is one of the top ten cited researchers in medicine and a Modern Healthcare magazine poll voted him the most influential physician leader in the US. He has published several textbooks and is the author of two bestsellers on the future of healthcare, The Creative Destruction of Medicine and The Patient Will See You Now. Please give a warm welcome to Dr. Eric Topol.</p>
                    </a>
                </div>
            </section>
            
            
<script>
$('.choices.speakers a').click(function(e) {
        e.preventDefault();
    $('span.minus', this ).toggle();
    $('span.plus', this ).toggle();
    $('p.bio', this ).toggle("slow");
    $('.choices.speakers a').toggle();
    $(this).show();
});
    
    
$('.day#day_one_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_one').show(); 
});
$('.day#day_two_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_two').show(); 
});
$('.day#day_three_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_three').show(); 
});
</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                                <a href="/home/scan">
                    <img src="<?=FRONT_ASSETS?>img/qr-code.png">
                    <p>RESOURCES</p>
                </a>
                <a href="/home/about">
                    <img src="<?=FRONT_ASSETS?>img/trivia.png">
                    <p>ABOUT US</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
<script type="text/javascript">
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('#video').removeClass('small_img');
            $('#share_alert').removeClass('small_check');
            $('#share_alert img').removeClass('small_check_img');
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);

            setTimeout(function(){
                $('#video').addClass('small_img');
                $('#share_alert').addClass('small_check');
                $('#share_alert img').addClass('small_check_img');
            }, 3500);

            
        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });



</script>


