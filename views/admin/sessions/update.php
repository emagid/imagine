<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			<input type="hidden" name="id" value="<?php echo $model->session->id; ?>" />
			<input name="token" type="hidden" value="<?php echo get_token();?>" />
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<h4>Session Details</h4>
						<div class="form-group">
                            <label>Name</label>
                            <?php echo $model->form->editorFor("name"); ?>
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <?php echo $model->form->editorFor("position"); ?>
                        </div>
						<div class="form-group">
                            <label>Title</label>
                            <?php echo $model->form->editorFor("title"); ?>
                        </div>
						<div class="form-group">
                            <label>Subtitle</label>
                            <?php echo $model->form->editorFor("subtitle"); ?>
                        </div>
						<div class="form-group">
                            <label>Date & Time</label> 
                            <input id="hccdatetime" type="text" value="" />
                            <input type="hidden" name="date_time" value="<?php echo $model->session->date_time; ?>" />
                        </div>
                        <div class="form-group">
                            <label>Text</label>
                            <?php echo $model->form->textAreaFor("text"); ?>
                        </div>
                        <div class="form-group">
                            <label>Headshot</label>
                            <p><input type="file" name="headshot" class='image'/></p>

                            <div style="display:inline-block">
                                <?php
                                $img_path = "";
                                if ($model->session->headshot != "" && file_exists(UPLOAD_PATH . 'sessions' . DS . $model->session->headshot)) {
                                    $img_path = UPLOAD_URL . 'sessions/' . $model->session->headshot;
                                    ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'sessions/delete_image/' . $model->session->id; ?>?type=headshot"
                                           onclick="return confirm('Are you sure?');"
                                           class="btn btn-default btn-xs">Delete</a>
                                        <input type="hidden" name="headshot"
                                               value="<?= $model->session->headshot ?>"/>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-save">Save</button>
	</div>
</form>

<?php footer(); ?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>

<script type="text/javascript">
	$('#hccdatetime').datetimepicker({
		format: "l, d M - h:i A",
		formatTime: "h:i A",
		step:10
	});
    $('#hccdatetime').val(moment.unix($('[name=date_time]').val()).format('dddd, DD MMM - hh:mm A'));
    $('#hccdatetime').change(function(){
       $('[name=date_time]').val(moment($('#hccdatetime').val(),'dddd, DD MMM - hh:mm A',true).utc().unix());
    }).change();
</script>