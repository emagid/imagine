<?php
if(count($model->sessions) > 0) { ?>
	<div class="box box-table">
		<table class="table">
			<thead>
				<tr>
					<th width=15%>Name</th>
					<th width=15%>Session Title</th>
					<th width=15%>SubTitle</th>
					<th width=10%>Date & Time</th>
					<th width="5%" class="text-center">Edit</th>
          			<th width="5%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model->sessions as $obj){  ?>
					<tr>
						<td><a href="<?php echo ADMIN_URL; ?>sessions/update/<?php echo $obj->id?>"><?php echo $obj->name;?></a></td>
						<td><a href="<?php echo ADMIN_URL; ?>sessions/update/<?php echo $obj->id?>"><?php echo $obj->title;?></a></td>
						<td><a href="<?php echo ADMIN_URL; ?>sessions/update/<?php echo $obj->id?>"><?php echo $obj->subtitle;?></a></td>
						<td><a class="dateTime" href="<?php echo ADMIN_URL; ?>sessions/update/<?php echo $obj->id?>"><?php echo $obj->date_time;?></a></td>
						<td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>sessions/update/<?= $obj->id ?>">
				           		<i class="icon-pencil"></i> 
				           	</a>
				        </td>
				        <td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>sessions/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
				           		<i class="icon-cancel-circled"></i> 
				           	</a>
				        </td>
					</tr>
				<? } ?>
			</tbody>
		</table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
	</div>
<? } else { ?>
	<h4>No Session details found. </h4>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'sessions';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
       $('.dateTime').each(function(){
          var text = $(this).text();
          $(this).text(moment.unix(text).format('dddd, DD MMM - hh:mm A'));
       });
    });
</script>